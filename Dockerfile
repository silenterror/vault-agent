# Start from Vault's official Docker image
FROM vault:latest

# Copy your agent-config.hcl file to the Docker image
COPY agent-config.hcl /etc/vault/agent-config.hcl

# Copy your ROLE_ID and SECRET_ID files
COPY role-id /vault/config/role-id
COPY secret-id /vault/config/secret-id

# Set the VAULT_ADDR environment variable. This should be the address of the Vault server.
ENV VAULT_ADDR=http://vault.vault.svc:8200

# Start Vault agent with the configuration file
CMD ["vault", "agent", "-config=/etc/vault/agent-config.hcl"]
